# CCJob
Job submission for lazy people.

CCJob offers:
* Templates for Q-Chem and Psi4
* Automatic submission for SLURM (`sbatch` style)
* Interactive submission for SLURM (`srun` style)
* Tools to create inputs and find files

