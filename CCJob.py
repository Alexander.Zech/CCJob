#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  9 15:38:55 2017

@author: alex
"""
import string
import os
import subprocess as sp
import shutil
import glob
import json
import re
from .Templates import defaults
from .Queue import queue_factory


def find_file(directory, extension="in"):
    """ Find input file in a directory.

    Returns
    -------
     : str or int
        Path to file or 0 in case of AssertionError.
    """
    dir_list = [fn for fn in glob.glob(directory+"/*."+extension)]
    try:
        assert len(dir_list) == 1
        return dir_list[0]
    except AssertionError:
        print("AssertionError: Could not determine single "+ extension +
              "file in " + directory + " !")
        return 0

def find_output(directory, extension="out"):
    """ Find output file in a directory.

    Returns
    -------
     : str or int
        Path to output file or 0 in case of AssertionError.
    """
    dir_list = [fn for fn in glob.glob(directory+"/*."+extension)
                if not os.path.basename(fn).startswith("slurm")]
    try:
        assert len(dir_list) == 1
        return dir_list[0]
    except AssertionError:
        print("AssertionError: Could not determine single .out \
              file in " + directory + " !")
        return 0

def find_eleconfig(directory):
    """Find a suitable electronic configuration file in a directory.

    Returns
    ----------
     : str or int
         Path to 'eleconfig.txt' or 0 in case of AssertionError.
    """
    usual_suspects = ["eleconfiguration.txt", "eleconfig.txt",
                      "elconfig.txt", "eleconf.txt", "econf.txt",
                      "elconf.txt", "ele.config", "electronic.conf",
                      "ccjob_elconfig.txt"
                     ]
    cand = [os.path.basename(fn) for fn in glob.glob(directory+"/*.txt")
            if os.path.isfile(fn)]
    cand.extend([os.path.basename(cf) for cf in glob.glob(directory+"/*.config")
                 if os.path.isfile(cf)])

    #intersec = usual_suspects & set(cand)
    intersec = [x for x in cand if x.lower() in usual_suspects]
    try:
        assert len(intersec) == 1
        return os.path.join(directory, intersec[0])
    except AssertionError:
        print("-- No or more than one elec. config file detected.")
#        print(("AssertionError: len(Candidate list) > 1 !\n"
#               "Please remove obsolete configuration files or "
#               "try saving the electronic configuration file using one of the"
#               "following file names:\n  - 'eleconf.txt'\n  - 'eleconfig.txt'"
#               "\n  - 'ele.config'"))
        return 0

def split_path(filepath):
    """
    Make absolute path and split filename from path.
    """
    dirname  = os.path.dirname(filepath)
    filename = os.path.basename(filepath)
    if len(dirname) == 0:
        # filepath is just a filename
        # set dirname to current working dir
        absdir = os.getcwd()
    else:
        absdir = os.path.abspath(dirname)
    return absdir, filename

def module_exists(module_name):
    """ Check if a module can be imported. """
    try:
        __import__(module_name)
    except ImportError:
        return False
    else:
        return True

class Input(object):
    def __init__(self, fpath, inp_string=None, to_file=True):
        self.input_string = inp_string

        # Caution!
        # Assuming $PWD for working directory if not
        # stated otherwise!
        wdir, infile = split_path(fpath)
        base, ext = os.path.splitext(infile)
        self.filepath = os.path.join(wdir, infile)
        self.wdir = wdir
        self.filename = infile
        self.extension = ext
        self.basename = base

        if to_file:
            self.save_input()

    @classmethod
    def from_template(cls, template, fpath, **kwargs):
        """ Alternative constructor using string.Template

        Parameters
        ----------
        template : string.Template
            Template of input file
        fpath : str
            Requested path to input file.
        **kwargs : key-value pairs
            keyworded arguments which have to match template
        """

        try:
            #use defaults first and overwrite with user's specs
            inp = template.substitute(defaults, **kwargs)
            return cls(fpath, inp_string=inp)
        except (KeyError, ValueError) as error:
            print(error)
            print(error.args)
            quit(0)

    @classmethod
    def from_file(cls, path_src, path_new=None):
        """ Alternative constructor just copying an existing input.
        """
        cp_cond = [os.path.exists(path_src), os.path.isfile(path_src),
                   len(path_new) != 0]
        content = ""

        # read input from file
        if cp_cond[0] and cp_cond[1]:
            with open(path_src) as f:
                content = f.read()

        # if path_new != None and all(cp_cond):
        #     # write old input to new file
        #     shutil.copy(path_src, path_new)
        #     if os.path.isdir(path_new):
        #         path_new = os.path.join(path_new, os.path.basename(
        #                 os.path.normpath(path_src)))
        #     return cls(path_new, inp_string=content, to_file=True)
        # else:

        # connect object with file content
        return cls(path_src, inp_string=content, to_file=False)

    def save_input(self):
        if not os.path.exists(self.wdir):
            os.makedirs(self.wdir)

        with open(self.filepath, "w") as f:
            f.write(self.input_string)
        print(f"-- Input file [{self.filename}] written successfully.")

class Job(object):
    """ Job Constructor.

        Holds job information.

        Parameters
        ----------
        ccinput : CCJob.Input
            CCJob.Input instance.
        mem : int
            Memory in MB.
        cpus : int
            Number of CPUs.
        time : string
            Time in the format that SLURM accepts (dd-hh, hh:mm:ss).
        partition : string
            Partition name.

        Returns
        -------
        CCJob
            CCJob instance.
        """

    # slurm_template = {"partition": "--partition=$partition",
    #                   "memory": "--mem=$memory",
    #                   "time": "--time=$time",
    #                   "cpus": "--cpus-per-task=$cpus",
    #                   "jobname": "--job-name=$jobname"
    #                   }
    def __init__(self, ccinput, script=None, queue="slurm", mem=500,
                 cpus=1, time="00:15:00", partition=None, jobname="CCJob",
                 software=None, meta_file="meta.json"):

        self.ccinput = ccinput
        self.script = script
        self.software = software
        if type(queue) == str:
            self.queue = queue_factory(queue)

        self.jobid = None

        self.meta = {"status": None,
            "wdir"     : self.ccinput.wdir,
            "infile"   : self.ccinput.filename,
            "basename" : self.ccinput.basename
            }
        self.meta_filename = os.path.basename(meta_file)
        self.meta_filepath = os.path.join(self.ccinput.wdir, meta_file)

        # submit options
        self.options = {"memory": mem, "cpus": cpus, "time": time,
                        "partition": partition, "jobname": jobname}
        self.custom_options = []
        self.software_options = []

    def get_job_options(self):
        """Prepare the string that holds all options for the queuing manager

        Returns
        -------
        argument : list
            Option string for queuing manager.
        """
        argument = [string.Template(self.queue.template[key]).substitute(
                    {key : value}) for key, value in self.options.items()]

        # if self.queue.lower() == "slurm":
        #     argument = [string.Template(Job.slurm_template[key]).substitute(
        #             {key : value}) for key, value in self.options.items()]
        # else:
        #     NotImplementedError("Currently the script only supports SLURM!")

        if len(self.custom_options) > 0:
            argument += self.custom_options

        return argument

    def set_custom_options(self, *args, use_long=True, silent=True, **kwargs):
        """Adds custom queueing manager options.

        Parameters
        ----------
        use_long : bool
            Whether to use long option names which are usually prepended with
            "`--`". The key and value pair is then separated by "=".
            (Default: ``True``)
        silent : bool
            Whether to print the custom options to screen.

        Other Parameters
        ----------------
        args   : tuple
            Tuple of additional flags (no key-value-pair).
        kwargs : dict
            Keywords and their values.

        """
        # TODO: need to automatically decide whether long or short format
        # keyworded arguments
        if use_long:
            self.custom_options.extend(["{0}={1}".format(k, v) for k, v in
                                        kwargs.items()])
        else:
            self.custom_options.extend(["{0} {1}".format(k, v) for k, v in
                                        kwargs.items()])
        # flags
        self.custom_options.extend(args)

        if not silent:
            print("-- Custom options specified: ", " ".join(self.custom_options))

    def set_software_options(self, output=None, output_extension="out",
                             prepend="-", *args, **kwargs):
        """Set options for software that should be run interactively.

        .. deprecated:: 1.0.0
            `set_software_options()` will be removed in CCJob version 1.1.0
        """
        cmd = []
        # let's add options first
        if kwargs is not None:
            cmd.extend([prepend+"{0} {1}".format(k, val) for k, val in
                        kwargs.items()])
        # add input filename
        cmd.append(self.ccinput.filename)
        # add output filename
        if output is None:
            cmd.append(self.ccinput.basename+"."+output_extension)
        else:
            cmd.append(output)
        # add anything else if needed
        if args is not None:
            cmd.append()
        self.software_options = cmd

    def is_running(self):
        """ Check whether job is still running.

        This function also changes the status variable. """
        # do we have a job ID to work with?
        if self.jobid == None:
            return False
        else:
            q_status = self.queue.get_status(self.jobid)

        if q_status == self.queue.state["active"]:
            self.meta["status"] = 'PENDING'
            return True
        else:
            return False

    def good_output(self, path_to_outfile,
                   success_string="Have a nice day.",
                   success_fct=None,
                   use_CCParser=True):
        """ Determines whether job terminated successfully.

        This function determines if the quantum chemistry software ended
        normally. It also changes the status variable.

        Parameters
        ----------
        path_to_outfile : str
            Absolute path to output file.
        success_string : str
            String to match in output regarding successful job completion
            (default: 'Have a nice day.').
        success_fct : function(path_to_output)
            Function object for custom parsing (default: None). Has to take
            output path as an input and has to return a boolean.
        use_CCParser : bool
            Use CCParser module if possible (defautl: True).
        """

        # do we have an output file?
        out_exists = os.path.exists(path_to_outfile)
        if not out_exists:
            # status then stays the default, i.e. 'None'
            return False

        normal = False
        # parse output file
        if use_CCParser and module_exists("CCParser"):
            import CCParser as ccp
            #get has_finished
            p = ccp.Parser(path_to_outfile, to_console=False, to_file=False)
            normal = p.results.has_finished[-1]
        else:
            # manual implementation of has_finished
            if success_fct == None:
                with open(path_to_outfile) as out:
                    for i, line in out:
                        if success_string in line:
                            normal = True
            else:
                normal = success_fct(path_to_outfile)
                if type(gg) != bool:
                    raise TypeError("Success_fct does not yield boolean!")

        if normal:
            self.meta["status"] = 'FIN'
        else:
            self.meta["status"] = 'FAIL'
        return normal

    def is_successful(self, out_extension='out',
                      success_string="Have a nice day.",
                      success_fct=None,
                      ignore_meta=False,
                      use_CCParser=True):
        """ Checks whether job finished with good output.

        In principle there are three cases to be considered:
         (1) Was the job completed and good output written to meta?
         (2) Is the job still running?
         (3) If it finished, did the quantum chemistry software end normally?

        The function changes the state of `self.meta["status"]` and possibly
        serializes `self.meta`.

        Parameters
        ----------
        out_extension : str
            File extension of output file (default: 'out').
        success_string : str
            String to match in output regarding successful job completion
            (default: 'Have a nice day.').
        success_fct : function(path_to_output)
            Function object for custom parsing (default: None). Has to take
            output path as an input and has to return a boolean.
        ignore_meta : bool
            Ignore existing meta file (default: False). Effectively overwrites
            old meta with new one.
        use_CCParser : bool
            Use CCParser module if possible (defautl: True).
        """
        # output info
        outfile = ".".join([self.meta["basename"], out_extension])
        path_to_out = os.path.join(self.meta["wdir"], outfile)

        # Case (1) - output checked previously
        is_fin = os.path.exists(self.meta_filepath) and \
                 self.load_status() == "FIN" and \
                 not ignore_meta

        if not is_fin:
            # Case (2) - active job
            is_running = self.is_running()
            successful = False
            if not is_running:
                # Case (3) - parse output
                successful = self.good_output(path_to_out,
                                        success_string=success_string,
                                        success_fct=success_fct,
                                        use_CCParser=use_CCParser)
            # update meta file
            self.save_meta()
            return not is_running and successful
        else:
            return True

    def submit(self, dry_run=False, silent=False):
        """Submit job to queuing manager in batch mode.

        Parameters
        ----------
        dry_run : bool
            Whether to perform a dry-run job submission (default: False).
        silent : bool
            Whether to print additional information (default: False).
        """
        q_arg = self.get_job_options()

        args = [self.queue.job_submit] \
               + q_arg \
               + [self.script, self.ccinput.filename]
        arg_str = " ".join(args)
        if dry_run:
            print("-- dry-run: ", arg_str)
        else:
            if not silent:
                print("-- running: ", arg_str)
            p = sp.run(arg_str, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
            out = p.stdout.decode("utf-8")
            if len(p.stderr) > 0:
                print("-- stderr: ", p.stderr)
            try:
                self.jobid = self.queue.parse_jobid_batch(out)
            except ValueError:
                print("!! Could not parse Job ID, showing stdout instead:")
                print("-- stdout: ", out)

    def smart_submit(self, dry_run=False, silent=False,
                     out_extension='out',
                     success_string="Have a nice day.",
                     success_fct=None,
                     ignore_meta=False,
                     use_CCParser=True):
        """ Safe-submit job based on meta conditions.

        In principle there are three cases to be considered:
         (1) Was the job completed and we already wrote it to meta?
         (2) Is the job still running?
         (3) If it finished, did the quantum chemistry software end normally?

        Parameters
        ----------
        dry_run : bool
            Whether to perform a dry-run job submission (default: False).
        silent : bool
            Whether to print additional information (default: False).
        out_extension : str
            File extension of output file (default: 'out').
        success_string : str
            String to match in output regarding successful job completion
            (default: 'Have a nice day.').
        success_fct : function(path_to_output)
            Function object for custom parsing (default: None). Has to take
            output path as an input and has to return a boolean.
        ignore_meta : bool
            Ignore existing meta file (default: False). Effectively overwrites
            old meta with new one.
        use_CCParser : bool
            Use CCParser module if possible (defautl: True).
        """
        # output info
        # outfile = ".".join([self.meta["basename"], out_extension])
        # path_to_out = os.path.join(self.meta["wdir"], outfile)
        #
        # # quick skip (meta knows that job terminated successfully)
        # quick_skip = os.path.exists(self.meta_filepath) and \
        #              self.load_status() == "FIN" and \
        #              not ignore_meta
        #
        #
        # if not quick_skip:
        #     running = self.is_running()
        #     if not running:
        #         # was calculation successful?
        #         successful = self.good_output(path_to_out,
        #                                 success_string=success_string,
        #                                 success_fct=success_fct)
        #         #
        #         if not successful:
        #             self.submit(dry_run=dry_run, silent=silent)
        #             self.meta["status"] = "PENDING"
        #
        #     # finally save status
        #     self.save_meta()

        if not self.is_successful(out_extension=out_extension,
                                  success_string=success_string,
                                  success_fct=success_fct,
                                  ignore_meta=ignore_meta,
                                  use_CCParser=use_CCParser):
            self.submit(dry_run=dry_run, silent=silent)
            self.meta["status"] = 'PENDING'
            self.save_meta()
        elif not silent:
            wdir = self.meta["wdir"]
            print(f"All good. Skipping folder {wdir}/")

    def run(self, dry_run=False, silent=False):
        """Submit job to queuing manager in live mode.

        This function uses the following commmand line structure:

        ``srun <SLURM options> script <input>``

        Parameters
        ----------
        dry_run : bool
            Whether to perform a dry-run job submission (default: False).
        silent : bool
            Whether to print additional information (default: False).
        """
        q_arg = self.get_job_options()

        args = [self.queue.job_run] \
               + q_arg \
               + [self.script, self.ccinput.filename]
        arg_str = " ".join(args)
        if dry_run:
            print("-- dry-run: ", arg_str)
        else:
            if not silent:
                print("-- running: ", arg_str)
            stdout = open("stdout.txt", "w")
            stderr = open("stderr.txt", "w")

            p = sp.run(arg_str, stdout=stdout, stderr=stderr, shell=True)

            stderr.close()
            stdout.close()

    def smart_run(self, dry_run=False, silent=False,
                  out_extension='out',
                  success_string="Have a nice day.",
                  success_fct=None,
                  ignore_meta=False,
                  use_CCParser=True):
        """ Safe-run job interactively based on meta conditions.

        Parameters
        ----------
        dry_run : bool
            Whether to perform a dry-run job submission (default: False).
        silent : bool
            Whether to print additional information (default: False).
        out_extension : str
            File extension of output file (default: 'out').
        success_string : str
            String to match in output regarding successful job completion
            (default: 'Have a nice day.').
        success_fct : function(path_to_output)
            Function object for custom parsing (default: None). Has to take
            output path as an input and has to return a boolean.
        ignore_meta : bool
            Ignore existing meta file (default: False). Effectively overwrites
            old meta with new one.
        use_CCParser : bool
                Use CCParser module if possible (defautl: True).
        """

        if not self.is_successful(out_extension=out_extension,
                                  success_string=success_string,
                                  success_fct=success_fct,
                                  ignore_meta=ignore_meta,
                                  use_CCParser=use_CCParser):
            self.run(dry_run=dry_run, silent=silent)
            self.meta["status"] = 'PENDING'
            self.save_meta()

    def save_meta(self):
        """Dump meta information in json format.
        """
        # jOut = os.path.join(self.meta["wdir"], meta_file)
        with open(self.meta_filepath, "w") as f:
            json.dump(self.meta, f)

    def load_status(self):
        """Read status from meta file.
        """
        # jIn = os.path.join(self.meta["wdir"], meta_file)
        with open(self.meta_filepath, "r") as f:
            tmp = json.load(f)
        if "status" in tmp.keys():
            # self.meta["status"] = tmp["status"]
            return tmp["status"]

###############################################################################
#   USEFUL TOOLS
###############################################################################
def zr_frag(fname, separator="----", fmt="string"):
    """ Get fragment coordinates from .zr file

    Parameters
    ----------
    fname: string
        ZR filename
    separator: string
        Separator.
    fmt: string
        Format specifier. Possible options: "list", "string"

    Returns
    -------
    frags: dict
        Dictionary of list of lists with fragment coordinates and atom symbol,
        e.g. ``frags["A"] = ["C 0.0 0.1 0.0", "..."]``. If no separator is
        found (normal xyz file), only one fragment is assumed ("AB").

        If fragments are found ``frags`` will contain the keys 'A', 'B', 'AB',
        'AB_ghost'.


    """
    if fmt.lower() not in ("string", "list"):
        raise ValueError("Invalid format option specified! Use either 'string' or 'list'.")
    with open(fname) as zr:
        rl = zr.readlines()
    line_B = 0
    frags = {}
    for i, line in enumerate(rl):
        if separator in line:
            line_B = i

    if line_B == 0:
        frags["AB"] = list(map(str.split, rl[0:]))
    else:
        frags["A"] = list(map(str.split, rl[0:line_B]))
        frags["B"] = list(map(str.split, rl[line_B+1:]))
        frags["AB_ghost"] = frags["A"] + list(map(lambda x: ["@"+x[0]]+x[1:],
                                                  frags["B"]))
        frags["BA_ghost"] = list(map(lambda x: ["@"+x[0]]+x[1:], frags["A"]))+\
                            frags["B"]
        frags["AB"] = frags["A"] + frags["B"]

    if fmt=="list":
        return frags
    elif fmt=="string":
        for key in frags:
#            frags[key] = "\n".join(["\t".join(s) for s in frags[key]])
#            "{0:<8}{1:<16}{2:<16}{3:<16}".format(*s)
            frags[key] = "\n".join(["    ".join(s) for s in frags[key]])
        return frags



def read_eleconfig(fname="eleconfig.txt", silent=False):
    """Read electronic configuration from file.

    Parameters
    ----------
    fname : str
        Input file name (default: 'eleconfig.txt').
    silent : bool
        Whether to print information to screen or not.

    Returns
    -------
    eleconfig : dict
        Dictionary containing the electronic configuration. If no file was
        found, an empty dictionary will be returned instead.
    """
    import re
    p_chg = r"charge_(?P<frag>[A-Za-z0-9]+)\s*=\s*(?P<value>[-+]?\d+)"
    p_mul = r"multiplicity_(?P<frag>[A-Za-z0-9]+)\s*=\s*(?P<value>\d+)"
    eleconfig = {}
    try:
        if fname == 0:
            raise FileNotFoundError
        with open(fname) as el:
            for x in el:
                m = re.search(p_chg, x)
                if m:
                    if m.group("frag") == "tot":
                        eleconfig["charge_tot"] = int(m.group("value"))
                    elif m.group("frag") in ["A", "a", "1"]:
                        eleconfig["charge_a"]   = int(m.group("value"))
                    elif m.group("frag") in ["B", "b", "2"]:
                        eleconfig["charge_b"]   = int(m.group("value"))
                m = re.search(p_mul, x)
                if m:
                    if m.group("frag") == "tot":
                        eleconfig["multiplicity_tot"] = int(m.group("value"))
                    elif m.group("frag") in ["A", "a", "1"]:
                        eleconfig["multiplicity_a"]   = int(m.group("value"))
                    elif m.group("frag") in ["B", "b", "2"]:
                        eleconfig["multiplicity_b"]   = int(m.group("value"))
        print("-- Obtained electronic configuration from file '" +fname+ "'.")
    except FileNotFoundError:
        print(("Could not find electronic configuration file. Using default "
               "values for charge and multiplicity (c=0, m=1)."))
    return eleconfig

def electronicConf(*dct, fname="eleconfig.txt"):
    """Update dictionary with charge and multiplicity from file.


    """
    eleconfig = read_eleconfig(fname)
    if len(dct) == 1:
        dct[0].update(eleconfig)
    else:
        return eleconfig
