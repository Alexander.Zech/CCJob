#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 18:34:00 2018

@author: Alexander Zech
"""
info = {"author" : "Alexander Zech",
        "version": "0.2.0",
        "date"   : "November 19th, 2018"}

import CCJob as ccj
import os
import numpy as np
import shutil
import re
import json
try:
    import CCParser as ccp
    has_ccp = True
except ImportError:
    has_ccp = False
###############################################################################

class NotConvergedError(Exception):
    pass

def print_iteration(it, lw=80):
    sw = 20
    q,m = divmod(lw-sw,2)
    print("*"*lw)
    print(" "*q, "Freeze-and-Thaw iteration #{0}".format(it))
    print("*"*lw)
    
def print_banner(string, lw=80):
    sw = len(string)
    q,m = divmod(lw-sw,2)
    print("*"*lw)
    print(" "*q, string)
    print("*"*lw)

def transform_AOmatrix(matrix, order):
    """Reorder an AO matrix given a certain order.
    
    Parameters
    ----------
    matrix : np.ndarray
        Input matrix in AO basis.
    order : list
        List of indices in new order.
        ``[0,1,2,...,n]`` will yield the same matrix again.
    
    Returns
    -------
    X : np.ndarray
        Reordered matrix.
    """
    nAO = matrix.shape[0]
    I = np.identity(nAO)
    P = I[:, order]
    X = np.dot(np.dot(P.T, matrix), P)
    return X

def read_density(filename, alpha_only=False, header=False, debug=False):
    """ Read density matrix from file
    
    Parameters
    ----------
    filename : str
        Input file name.
    alpha_only : bool
        Whether the file contains one density matrix.
    header : bool
        Whether the file contains a header in the first line.
    debug : bool
        Whether to print status information.
    """
    skip = 0 if not header else 1
    D = np.loadtxt(filename, dtype=np.float64, skiprows=skip)
    if alpha_only:
        nAO = int(np.sqrt(len(D)))
        D = np.array([D.reshape((nAO, nAO)),D.reshape((nAO, nAO))])
    else:
        nAO = int(np.sqrt(len(D)/2))
        D = D.reshape((2, nAO, nAO))# numpy iterates over left-most axis first
    if debug:
        print("-- read_density determined {0} basis functions".format(nAO))
    return D

def save_density(filename, X, alpha_only=False, header=False, debug=False):
    major, minor = list(map(int, np.__version__.split(".")[0:2]))
    major_good, minor_good = 1, 15
    if major >= major_good and minor >= minor_good:
        save_density_np(filename, X, alpha_only=alpha_only, header=header,
                        debug=debug)
    else:
        print("-- Detected numpy version < 1.15.x ! Using save_density_legacy...")
        save_density_legacy(filename, X, alpha_only=alpha_only, header=header,
                        debug=debug)

def save_density_np(filename, X, alpha_only=False, header=False, debug=False):
    """Save density matrix tensor using numpy.
    
    Parameters
    ----------
    filename : str
        Output file name.
    X : np.ndarray
        (Density) matrix
    alpha_only : bool
        Whether only the alpha density matrix should be written to file.
    header : bool
        Whether to print a header to file or not.
    debug : bool
        Whether to print status information.
    """
    h = "" if not header else " ".join([i for i in X.shape])
    with open(filename, "w") as f:
        if alpha_only:
            np.savetxt(f, X, delimiter="\n", fmt="%14.10e", header=h)
        else:
            for slice_2d in X:
                np.savetxt(f, slice_2d, delimiter="\n", fmt="%14.10e",
                           header=h)
                
def save_density_legacy(filename, X, alpha_only=False, header=False,
                        debug=False):
    """Save density matrix tensor if numpy version is too old (legacy mode).
    
    Parameters
    ----------
    filename : str
        Output file name.
    X : np.ndarray
        (Density) matrix
    alpha_only : bool
        Whether only the alpha density matrix should be written to file.
    header : bool
        Whether to print a header to file or not.
    debug : bool
        Whether to print status information.
    """
    h = "" if not header else " ".join([i for i in X.shape])
    # determine number of AOs from shape --> either (nAO, nAO) or (2, nAO, nAO)
    rows = X.shape[0] if alpha_only else X.shape[1]
    cols = rows
    with open(filename, "w") as f:
        if header:
            f.write(h)
        if alpha_only:
            for i in range(rows):
                for j in range(cols):
                    f.write("{0:.10e}\n".format(X[i,j]))
        else:
            for i in range(2):
                for j in range(rows):
                    for k in range(cols):
                        f.write("{0:.10e}\n".format(X[i,j,k]))

def copy_density(src, dest, fragment="A", debug=False):
    """Copy the density matrix file to a new location as 'Densmat_A.txt' or
    'Densmat_B.txt'.
    
    Parameters
    ----------
    src : str
        Source file.
    dest : str
        Destination folder.
    fragment : str
        Fragment identifier ['A' or 'B']. Default: 'A'
    """
    if fragment not in ["A","B"]:
        raise ValueError("Invalid fragment specifier")
    fname = "Densmat_{0}.txt".format(fragment)
    # Check if destination folder exists
    if not os.path.exists(dest):
        os.makedirs(dest)
    # Check if source exists and proceed
    if os.path.exists(src) and os.path.isfile(src):
        shutil.copy(src, os.path.join(dest, fname))
        if debug:
            print("-- copied "+fname+" successfully.")

def get_templates(method_active, isXC):
    """Quick way to get the templates we need. Extend if necessary!
    
    Parameters
    ----------
    method_active : str
        Method name for active species.
    isXC : bool
        Whether we specify XC or X+C separately.
        
    Returns
    -------
    templates : list
        List of three templates (no_import, import_B, import_AB).
    
    """
    templates = [None, None, None]
    if method_active == "HF":
        if isXC:
            from CCJob.Templates import HFinHF, HFinHF_impB, HFinHF_impAB
            templates[0] = HFinHF
            templates[1] = HFinHF_impB
            templates[2] = HFinHF_impAB
        else:
            from CCJob.Templates import HFinHF_X_C, HFinHF_impB_X_C, HFinHF_impAB_X_C
            templates[0] = HFinHF_X_C
            templates[1] = HFinHF_impB_X_C
            templates[2] = HFinHF_impAB_X_C
    elif method_active == "MP2":
        if isXC:
            from CCJob.Templates import ADCinHF, ADCin_impB, ADCin_impAB
            templates[0] = ADCinHF
            templates[1] = ADCin_impB
            templates[2] = ADCin_impAB
        else:
            from CCJob.Templates import ADCinHF_X_C, ADCin_impB_X_C, ADCin_impAB_X_C
            templates[0] = ADCinHF_X_C
            templates[1] = ADCin_impB_X_C
            templates[2] = ADCin_impAB_X_C
    return templates

def load_json(path_to_json):
    """ Load a JSON file """
    with open(path_to_json, "r") as f:
        data = json.load(f)
    return data

def get_embedded_energy(outfile, with_ccp=False):
    """ Get embedded energy of subsystem A.

    Parameters
    ----------
    outfile : :obj:`str`
        Filename of output to be parsed
    with_ccp : :obj:`bool`
        Whether or not to use CCParser
    """
    E_A = []
    if with_ccp:
        # Parse output
        out = ccp.Parser(outfile, to_console=False,
                to_file=True, to_json=True,
                log_file="ccp_{0}.log".format(outfile[:-4]),
                json_file="ccp_{0}.json".format(outfile[:-4]))
        E_A = out.results.E_A.data
    else:
        with open(outfile) as f:
            for i, line in enumerate(f):
                if "Embedded System (A):" in line:
                    E_A.append(float(line.split()[-2]))

    # the order is generally HF(0), post-HF(1->nstates)
    if len(E_A) <= 2:
        return E_A[-1]
    elif len(E_A) > 2:
        # if for some reason we also calculate excited states
        return E_A[1]
def get_nbas(outfile, with_ccp=False, index=[2,3,4]):
    """ Get number of basis functions"""
    nbas = {"nbasA" : 0, "nbasB": 0, "nbasAB": 0}
    if with_ccp:
        # check if json file exists
        json_file="ccp_{0}.json".format(outfile[:-4])
        if os.path.exists(json_file):
            parsed = load_json(json_file)
            nbas["nbasAB"] = parsed["nbas"][index[0]][0]
            nbas["nbasA"]  = parsed["nbas"][index[1]][0]
            nbas["nbasB"]  = parsed["nbas"][index[2]][0]
        else:
            out = ccp.Parser(outfile, to_console=False,
                    to_file=True, to_json=True,
                    log_file="ccp_{0}.log".format(outfile[:-4]),
                    json_file="ccp_{0}.json".format(outfile[:-4]))
            nbas["nbasAB"] = out.results.nbas[2]
            nbas["nbasA"]  = out.results.nbas[3]
            nbas["nbasB"]  = out.results.nbas[4]
    else:
        with open(outfile) as f:
            r = f.read()
        pattern = r"There are (?P<shells>\d+) shells and (?P<bsf>\d+) basis functions"
        parsed  = re.findall(pattern, r)
        nbas["nbasAB"] = parsed[index[0]][1]
        nbas["nbasA"]  = parsed[index[1]][1]
        nbas["nbasB"]  = parsed[index[2]][1]
    return nbas
                        
def freezeAndThaw(specs, queue, specs_B=None, queue_B=None, method_A="HF",
                  method_B="HF", maxiter=15, thresh=1e-8, use_zr=True, lw=80,
                  B_init=False, q_custom=None):
    """Run FDET freeze-and-thaw iterations.
    
    Parameters
    ----------
    specs : :obj:`dict`
        Dictionary containing values to create the input from template.
    queue : :obj:`dict`
        Dictionary for queuing manager variables (see :class:`CCJob.Job`).
    specs_B : :obj:`dict`
        Same as ``specs`` but for system B (optional, default=None).
    queue_B : :obj:`dict`
        Same as ``queue`` but for system B (optional, default=None).
    method_A : :obj:`str`
        Quantum chemical method abbrev. for system A (default: 'HF').
    method_B : :obj:`str`
        Quantum chemical method abbrev. for system B (default: 'HF').
    maxiter : :obj:`int`, optional
        Maximum number of iterations (default: 15).
    thresh : :obj:`float`, `optional`
        Freeze-and-thaw threshold w.r.t. Energy of A (default: 1e-8).
    use_zr : :obj:`bool`, optional
        Whether to obtain fragment structures from ZR file (default: True).
    lw : :obj:`int`
        Line width (for print function)
    B_init : :obj:`bool`
        Whether an initial density matrix for B is provided (headerless, alpha+beta).
    q_custom : :obj:`dict`, optional
        Custom options for queuing manager (see
        :func:`CCJob.Job.set_custom_options`)
      
    Notes
    -----
    The module depends on the following non-standard module which should be
    in the $PYTHONPATH:
        
    ``CCJob``
    
    Warning
    -------
    A common pitfall is a crash due to wrong charge and multiplicity with the 
    automatically generated inputs! (default: charge=0, multiplicity=1)
    
    
    """
    # Warning
    print("-"*lw)
    print("[!!!] In the FT folder you should have:")
    print("     - Structure file      '*.zr'          (if 'use_zr=True')")
    print("     - Density matrix of B 'Densmat_B.txt' (if 'B_init=True')")
    print("     - Electr. Config.     'eleconfig.txt' (optional)")
    print("-"*lw)
    
    # Check inputs
    if "expansion" not in specs.keys():
        raise KeyError("FDET basis expansion not defined!!")
    elif specs["expansion"] not in ["ME", "SE"]:
        raise ValueError(("Wrong specifier for FDET basis expansion."
                         " Use 'SE' or 'ME' only."))
    if method_A not in ["HF", "MP2"]:
        raise ValueError("Descriptor for 'method_A' not recognized!")
    elif method_B not in ["HF", "MP2"]:
        raise ValueError("Descriptor for 'method_B' not recognized!")
    
    # General
    isXC = False if not "xc_func" in specs.keys() else True
    cm_keys = ["charge_a", "charge_b", "charge_tot", "multiplicity_a",
               "multiplicity_b", "multiplicity_tot"]
    hasElConf = all([x in specs.keys() for x in cm_keys])
    
    # Iteration related
    it = 0
    thresh_fnt = thresh
    max_iter = maxiter
    energy = []
    
    # Files and folders
    iterDirName = "cy"
    iterFilName = "fnt_emb"
    densMat_from_FDE = "FDE_State0_tot_dens.txt"
    if B_init:
        densMat_B = "Densmat_B.txt"
    else:
        densMat_B = "frag1_{0}_dens_{1}.txt".format(method_B, specs["expansion"])
    
    # Important paths
    root = os.getcwd()
    path_cur = os.path.join(root, iterDirName + str(it))
    
    #--------------------------------------------------------------------------
    #    DETERMINE STRUCTURE AND EL. CONFIGURATION
    #--------------------------------------------------------------------------
    print_banner("Molecular structure and electr. configuration")

    # XYZ structures (default: from .zr file)
    if use_zr:
        print("-- XYZ from ZR file")
        zr_file = ccj.find_file(root, extension="zr")
        frags = ccj.zr_frag(zr_file)
        specs.update(dict(frag_a=frags["A"], frag_b=frags["B"]))
    else:
        print("-- XYZ from function input")
    natmA = len(specs["frag_a"].split("\n"))
    natmB = len(specs["frag_b"].split("\n"))
    print("-- Number of atoms (A): ", natmA)
    print("-- Number of atoms (B): ", natmB)
    if natmA == 0 or natmB == 0:
        raise ValueError("Atomless fragment detected! Check your coordinates!")

    # try to get electronic configuration
    if not hasElConf:
        f_elconf = ccj.find_eleconfig(root)
        eleconfig = ccj.read_eleconfig(fname=f_elconf)
        specs.update(eleconfig)
        hasElConf = len(eleconfig) > 0
    print("-- Charge (A)      : ", 0 if not hasElConf else specs["charge_a"])
    print("-- Multiplicity (A): ", 1 if not hasElConf else specs["multiplicity_a"])
    print("-- Charge (B)      : ", 0 if not hasElConf else specs["charge_b"])
    print("-- Multiplicity (B): ", 1 if not hasElConf else specs["multiplicity_b"])
    
    # Create and change into first folder
    if not os.path.exists(path_cur):
        os.makedirs(path_cur)
    os.chdir(path_cur)
    #--------------------------------------------------------------------------
    #         FIRST JOB
    #--------------------------------------------------------------------------
    print_iteration(it)
    
    # Prepare first input
    t_idx    = 0 if not B_init else 1
    curr_inp = iterFilName+".in"
    curr_job = "FT-{0}".format(it)
    
    # copy density matrix (B)
    if B_init:
        shutil.copy(os.path.join(root, densMat_B), path_cur)
        
    templates = get_templates(method_A, isXC)
    inp = ccj.Input.from_template(templates[t_idx], curr_inp, **specs)
    
    # Prepare job 
    queue["jobname"] = curr_job
    job = ccj.Job(inp, **queue)
    if q_custom != None:
        job.set_custom_options(**q_custom)
    
    # Run the first job
    job.run()
    
    # get embedded energy of active subsystem
    E_embA = get_embedded_energy(iterFilName+".out", with_ccp=has_ccp)
    energy.append(E_embA)
    
    # get number of basis functions from the basis initializer block (constructor)
    #for ME the number of basis is not used. could be moved into "if SE" unless wanted as check
    nbas = get_nbas(iterFilName+".out", with_ccp=has_ccp)  
    nbasAB = nbas["nbasAB"]
    nbasA  = nbas["nbasA"]
    nbasB  = nbas["nbasB"]
    print("""-- Found number of basis functions:
       .nbasAB = {0}
       .nbasA  = {1}
       .nbasB  = {2}
    """.format(nbasAB, nbasA, nbasB))
    
    # --- RE-ORDER DENSITY MATRICES ---
    A = read_density(densMat_from_FDE, debug=True)
    hd, alp = (not B_init), (not B_init)
    B = read_density(densMat_B, header=hd, alpha_only=alp)
    
    # transform each nAO x nAO matrix if supermolecular expansion
    if specs["expansion"] == "SE":
        A_tf = np.zeros((2, nbasAB, nbasAB)) # density that serves as rhoB in next it
        B_tf = np.zeros((2, nbasAB, nbasAB))
        number_line = [i for i in range(nbasAB)]
        order_to_B  = number_line[nbasA:] + number_line[:nbasA]
        order_to_A  = number_line[nbasB:] + number_line[:nbasB]
        for i in range(2):
            A_tf[i] = transform_AOmatrix(A[i], order=order_to_B)
            B_tf[i] = transform_AOmatrix(B[i], order=order_to_B)
    else:
        A_tf = A
        B_tf = B
    
    # adjust iteration counter
    it += 1
    # make a copy of A's specs dictionary
    specsA = dict(**specs)
    #------------------------------------------------------------------------------
    #         ALL OTHER JOBS
    #------------------------------------------------------------------------------
    difference_to_last = 1
    while abs(difference_to_last) > thresh_fnt and it < max_iter:
        # iteration variables
        is_A = bool((it+1) % 2)
        print_iteration(it)
        curr_inp = iterFilName+".in"
        curr_job = "FT-{0}".format(it)
        
        # Folders
        path_cur = os.path.join(root, iterDirName + str(it))
        if not os.path.exists(path_cur):
            os.makedirs(path_cur)
        os.chdir(path_cur)
        
        # save transformed density matrix from last iteration
        if is_A:
            save_density("Densmat_B.txt", B_tf)
            save_density("Densmat_A.txt", A_tf)
        else:
            save_density("Densmat_B.txt", A_tf)
            save_density("Densmat_A.txt", B_tf)
            
        # reverse order (embedded species is always "frag_a")
        if is_A:
            specs.update(specsA)
        else:
            specs["frag_a"] = specsA["frag_b"]
            specs["frag_b"] = specsA["frag_a"]
            if hasElConf:
                specs["charge_a"] = specsA["charge_b"]
                specs["charge_b"] = specsA["charge_a"]
                specs["multiplicity_a"] = specsA["multiplicity_b"]
                specs["multiplicity_b"] = specsA["multiplicity_a"]
            
        # Prepare input & job
        if not is_A and specs_B != None and queue_B != None:
            queue_B["jobname"] = curr_job
            inp = ccj.Input.from_template(templates[2], curr_inp,
                                          frag_a=specsA["frag_b"],
                                          frag_b=specsA["frag_a"], **specs_B)
            job = ccj.Job(inp, **queue_B)
        else:
            queue["jobname"] = curr_job
            inp = ccj.Input.from_template(templates[2], curr_inp, **specs)
            job = ccj.Job(inp, **queue)
            
        if q_custom != None:
            job.set_custom_options(**q_custom)
          
        # Run the first job
        job.run()
        
        # get embedded energy of active subsystem
        E_embA = get_embedded_energy(iterFilName+".out", with_ccp=has_ccp)
        energy.append(E_embA)
            
        # print current residual
        if is_A:
            difference_to_last = energy[it] - energy[it-2]
            print("-- Current residual: {0: .8e}".format(difference_to_last))
        
        # read embedded density from file: D.shape = (2, nAO, nAO)
        A = read_density(densMat_from_FDE)
        if specs["expansion"] == "SE":
            for i in range(2):
                if is_A:
                    A_tf[i] = transform_AOmatrix(A[i], order=order_to_B)
                    B_tf[i] = transform_AOmatrix(B_tf[i], order=order_to_B)
                else:
                    A_tf[i] = transform_AOmatrix(A_tf[i], order=order_to_A)
                    B_tf[i] = transform_AOmatrix(A[i], order=order_to_A)
        else:
            if is_A:
                A_tf = A
            else:
                B_tf = A
        
        # increase iteration counter
        it += 1
        # END WHILE LOOP
    if it >= max_iter:
        raise NotConvergedError("Freeze-and-Thaw cycles did not converge in "
                                "{0} iterations!".format(max_iter))
    else:
        print("*"*lw)
        print(("[!] Finished Freeze-and-Thaw calculation successfully in "
               "{0} iterations.".format(it)))
        
def freezeAndThaw_general(specs, queue, specs_B=None, queue_B=None, method_A="HF",
                  method_B="HF", maxiter=15, thresh=1e-8, use_zr=True, lw=80,
                  B_init=False, q_custom=None):
    """Run FDET freeze-and-thaw iterations.
    
    Parameters
    ----------
    specs : :obj:`dict`
        Dictionary containing values to create the input from template.
    queue : :obj:`dict`
        Dictionary for queuing manager variables (see :class:`CCJob.Job`).
    specs_B : :obj:`dict`
        Same as ``specs`` but for system B (optional, default=None).
    queue_B : :obj:`dict`
        Same as ``queue`` but for system B (optional, default=None).
    method_A : :obj:`str`
        Quantum chemical method abbrev. for system A (default: 'HF').
    method_B : :obj:`str`
        Quantum chemical method abbrev. for system B (default: 'HF').
    maxiter : :obj:`int`, optional
        Maximum number of iterations (default: 15).
    thresh : :obj:`float`, `optional`
        Freeze-and-thaw threshold w.r.t. Energy of A (default: 1e-8).
    use_zr : :obj:`bool`, optional
        Whether to obtain fragment structures from ZR file (default: True).
    lw : :obj:`int`
        Line width (for print function)
    B_init : :obj:`bool`
        Whether an initial density matrix for B is provided (headerless, alpha+beta).
    q_custom : :obj:`dict`, optional
        Custom options for queuing manager (see
        :func:`CCJob.Job.set_custom_options`)
      
    Notes
    -----
    The module depends on the following non-standard module which should be
    in the $PYTHONPATH:
        
    ``CCJob``
    
    Warning
    -------
    A common pitfall is a crash due to wrong charge and multiplicity with the 
    automatically generated inputs! (default: charge=0, multiplicity=1)
    
    
    """
    # Warning
    print("-"*lw)
    print("[!!!] In the FT folder you should have:")
    print("     - Structure file      '*.zr'          (if 'use_zr=True')")
    print("     - Density matrix of B 'Densmat_B.txt' (if 'B_init=True')")
    print("     - Electr. Config.     'eleconfig.txt' (optional)")
    print("-"*lw)
    
    # Check inputs
    if "expansion" not in specs.keys():
        raise KeyError("FDET basis expansion not defined!!")
    elif specs["expansion"] not in ["ME", "SE"]:
        raise ValueError(("Wrong specifier for FDET basis expansion."
                         " Use 'SE' or 'ME' only."))
    if method_A not in ["HF", "MP2"]:
        raise ValueError("Descriptor for 'method_A' not recognized!")
    elif method_B not in ["HF", "MP2"]:
        raise ValueError("Descriptor for 'method_B' not recognized!")
    elif method_B != method_A:
        raise ValueError("different methods not tested yet!!")
    
    # General
    isXC = False if not "xc_func" in specs.keys() else True
    cm_keys = ["charge_a", "charge_b", "charge_tot", "multiplicity_a",
               "multiplicity_b", "multiplicity_tot"]
    hasElConf = all([x in specs.keys() for x in cm_keys])
    
    # Iteration related
    it = 0
    thresh_fnt = thresh
    max_iter = maxiter
    energy = []
    
    # Files and folders
    iterDirName = "cy"
    iterFilName = "fnt_emb"
    densMat_from_FDE = "FDE_State0_tot_dens.txt"
    if B_init:
        densMat_B = "Densmat_B.txt"
    elif method_B=="HF":
        densMat_B = "frag1_HF_dens_{0}.txt".format(specs["expansion"])
    elif method_B=="MP2":
        densMat_B = "Densmat_B.txt"
    
    # Important paths
    root = os.getcwd()
    path_cur = os.path.join(root, iterDirName + str(it))
    
    #--------------------------------------------------------------------------
    #    DETERMINE STRUCTURE AND EL. CONFIGURATION
    #--------------------------------------------------------------------------
    print_banner("Molecular structure and electr. configuration")

    # XYZ structures (default: from .zr file)
    if use_zr:
        print("-- XYZ from ZR file")
        zr_file = ccj.find_file(root, extension="zr")
        frags = ccj.zr_frag(zr_file)
        specs.update(dict(frag_a=frags["A"], frag_b=frags["B"]))
    else:
        print("-- XYZ from function input")
    natmA = len(specs["frag_a"].split("\n"))
    natmB = len(specs["frag_b"].split("\n"))
    print("-- Number of atoms (A): ", natmA)
    print("-- Number of atoms (B): ", natmB)
    if natmA == 0 or natmB == 0:
        raise ValueError("Atomless fragment detected! Check your coordinates!")

    # try to get electronic configuration
    if not hasElConf:
        f_elconf = ccj.find_eleconfig(root)
        eleconfig = ccj.read_eleconfig(fname=f_elconf)
        specs.update(eleconfig)
        hasElConf = len(eleconfig) > 0
    print("-- Charge (A)      : ", 0 if not hasElConf else specs["charge_a"])
    print("-- Multiplicity (A): ", 1 if not hasElConf else specs["multiplicity_a"])
    print("-- Charge (B)      : ", 0 if not hasElConf else specs["charge_b"])
    print("-- Multiplicity (B): ", 1 if not hasElConf else specs["multiplicity_b"])
    
    # Create and change into first folder
    if not os.path.exists(path_cur):
        os.makedirs(path_cur)
    os.chdir(path_cur)
    #------------------------------------------inp--------------------------------
    #         FIRST JOB
    #--------------------------------------------------------------------------
    print_iteration(it)
    
    # Prepare first input
    t_idx    = 0 if not B_init else 1
    curr_inp = iterFilName+".in"
    curr_job = "FT-{0}".format(it)
    
    # copy density matrix (B), if provided
    if B_init:
        shutil.copy(os.path.join(root, densMat_B), path_cur)
        
    templates = get_templates(method_A, isXC)
    
    if method_B == "MP2":
        if isXC:
            from CCJob.Templates import ADCin_MP2
            templates[0]=ADCin_MP2
        else:
            from CCJob.Templates import ADCin_MP2_X_C
            templates[0]=ADCin_MP2_X_C
    if specs["expansion"]=="ME":
        specs["frag_b_bse"]=specs["frag_b"]
    elif specs["expansion"]=="SE":
        specs["frag_b_bse"] = "@"+"\n@".join(specs["frag_a"].split("\n"))+"\n"+specs["frag_b"]
                            
    inp = ccj.Input.from_template(templates[t_idx], curr_inp, **specs)
    
    # Prepare job 
    queue["jobname"] = curr_job
    job = ccj.Job(inp, **queue)
    if q_custom != None:
        job.set_custom_options(**q_custom)
    
    # Run the first job
    job.run()
    
    # get embedded energy of active subsystem
    E_embA = get_embedded_energy(iterFilName+".out", with_ccp=has_ccp)
    energy.append(E_embA)
    

    
    # --- RE-ORDER DENSITY MATRICES ---
    A = read_density(densMat_from_FDE, debug=True)
    hd, alp = method_B=="HF" and not B_init, method_B=="HF" and not B_init
    B = read_density(densMat_B, header=hd, alpha_only=alp)
    
    # transform each nAO x nAO matrix if supermolecular expansion
    if specs["expansion"] == "SE":
        # get number of basis functions from the basis initializer block (constructor)
        indx=[3,4,5] if specs["expansion"]=="SE" and method_A=="MP2" and not B_init else [2,3,4]
        nbas = get_nbas(iterFilName+".out", with_ccp=has_ccp,index=indx)
        nbasAB = nbas["nbasAB"]
        nbasA  = nbas["nbasA"]
        nbasB  = nbas["nbasB"]
        print("""-- Found number of basis functions:
           .nbasAB = {0}
           .nbasA  = {1}
           .nbasB  = {2}
        """.format(nbasAB, nbasA, nbasB))
        A_tf = np.zeros((2, nbasAB, nbasAB)) # density that serves as rhoB in next it
        B_tf = np.zeros((2, nbasAB, nbasAB))
        number_line = [i for i in range(nbasAB)]
        order_to_B  = number_line[nbasA:] + number_line[:nbasA]
        order_to_A  = number_line[nbasB:] + number_line[:nbasB]
        for i in range(2):
            A_tf[i] = transform_AOmatrix(A[i], order=order_to_B)
            B_tf[i] = transform_AOmatrix(B[i], order=order_to_B)
    else:
        A_tf = A
        B_tf = B
    
    # adjust iteration counter
    it += 1
    # make a copy of A's specs dictionary
    specsA = dict(**specs)
    #------------------------------------------------------------------------------
    #         ALL OTHER JOBS
    #------------------------------------------------------------------------------
    difference_to_last = 1
    while abs(difference_to_last) > thresh_fnt and it < max_iter:
        # iteration variables
        is_A = bool((it+1) % 2)
        print_iteration(it)
        curr_inp = iterFilName+".in"
        curr_job = "FT-{0}".format(it)
        
        # Folders
        path_cur = os.path.join(root, iterDirName + str(it))
        if not os.path.exists(path_cur):
            os.makedirs(path_cur)
        os.chdir(path_cur)
        
        # save transformed density matrix from last iteration
        if is_A:
            save_density("Densmat_B.txt", B_tf)
            save_density("Densmat_A.txt", A_tf)
        else:
            save_density("Densmat_B.txt", A_tf)
            save_density("Densmat_A.txt", B_tf)
            
        # reverse order (embedded species is always "frag_a")
        if is_A:
            specs.update(specsA)
        else:
            specs["frag_a"] = specsA["frag_b"]
            specs["frag_b"] = specsA["frag_a"]
            if hasElConf:
                specs["charge_a"] = specsA["charge_b"]
                specs["charge_b"] = specsA["charge_a"]
                specs["multiplicity_a"] = specsA["multiplicity_b"]
                specs["multiplicity_b"] = specsA["multiplicity_a"]
            
        # Prepare input & job
        if not is_A and specs_B != None and queue_B != None:
            queue_B["jobname"] = curr_job
            inp = ccj.Input.from_template(templates[2], curr_inp,
                                          frag_a=specsA["frag_b"],
                                          frag_b=specsA["frag_a"], **specs_B)
            job = ccj.Job(inp, **queue_B)
        else:
            queue["jobname"] = curr_job
            inp = ccj.Input.from_template(templates[2], curr_inp, **specs)
            job = ccj.Job(inp, **queue)
            
        if q_custom != None:
            job.set_custom_options(**q_custom)
          
        # Run the first job
        job.run()
        
        # get embedded energy of active subsystem
        E_embA = get_embedded_energy(iterFilName+".out", with_ccp=has_ccp)
        energy.append(E_embA)
            
        # print current residual
        if is_A:
            difference_to_last = energy[it] - energy[it-2]
            print("-- Current residual: {0: .8e}".format(difference_to_last))
        
        # read embedded density from file: D.shape = (2, nAO, nAO)
        A = read_density(densMat_from_FDE)
        if specs["expansion"] == "SE":
            for i in range(2):
                if is_A:
                    A_tf[i] = transform_AOmatrix(A[i], order=order_to_B)
                    B_tf[i] = transform_AOmatrix(B_tf[i], order=order_to_B)
                else:
                    A_tf[i] = transform_AOmatrix(A_tf[i], order=order_to_A)
                    B_tf[i] = transform_AOmatrix(A[i], order=order_to_A)
        else:
            if is_A:
                A_tf = A
            else:
                B_tf = A
        
        # increase iteration counter
        it += 1
        # END WHILE LOOP
    if it >= max_iter:
        raise NotConvergedError("Freeze-and-Thaw cycles did not converge in "
                                "{0} iterations!".format(max_iter))
    else:
        print("*"*lw)
        print(("[!] Finished Freeze-and-Thaw calculation successfully in "
               "{0} iterations.".format(it)))
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#if __name__ == "__main__":
#    import argparse
#    arg_parser = argparse.ArgumentParser(description=("Run Freeze-and-Thaw"
#                                         " calculations with Q-Chem."))
#    # positional arguments
#    #arg_parser.add_argument("outFiles", metavar="out", nargs="+", help="outputfiles")
#    # FDET basis expansion
#    arg_parser.add_argument("-S", "--super", action="store_const",
#                            const="SE", default="ME", dest="expansion",
#                            help=("use supermol. expansion (default: monomer"
#                            " expansion)"))
#    # FnT threshold
#    arg_parser.add_argument("-t", "--threshold", action="store", default=1e-6,
#                            type=float, dest="thr",
#                            help=("Threshold for change of Energyof A "
#                                  "(default: 1e-6)"))
#    # FnT maxiter
#    arg_parser.add_argument("-i", "--maxiter", action="store", default=12,
#                            type=int, dest="maxiter",
#                            help=("set maximum freeze-and-thaw iterations "
#                                  "(default: 12)"))
#    # version info
#    arg_parser.add_argument("-v", "--version", action="version",
#                        version="%(prog)s | Version: {0}\n {1}, {2}".format(
#                                info["version"], info["author"], info["date"]),
#                        help="show version info")
#    args = arg_parser.parse_args()
#
#
#    run_freeze_and_thaw(expansion=args.expansion, maxiter=args.maxiter, 
#                        thresh=args.thr)
    
