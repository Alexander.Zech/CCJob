#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 18:34:00 2018

@author: Alexander Zech
"""
info = {"author" : "Alexander Zech",
        "version": "0.1.0",
        "date"   : "November 19th, 2018"}

from .freeze_and_thaw import print_iteration, print_banner, get_templates
import CCJob as ccj
import CCParser as ccp
import os
#import numpy as np
import shutil


#def print_iteration(it, lw=80):
#    sw = 20
#    q,m = divmod(lw-sw,2)
#    print("*"*lw)
#    print(" "*q, "Macrocycle iteration #{0}".format(it))
#    print("*"*lw)
#    
#def print_banner(string, lw=80):
#    sw = len(string)
#    q,m = divmod(lw-sw,2)
#    print("*"*lw)
#    print(" "*q, string)
#    print("*"*lw)
#
#def transform_AOmatrix(matrix, order):
#    """Reorder an AO matrix given a certain order.
#    
#    Parameters
#    ----------
#    matrix : np.ndarray
#        Input matrix in AO basis.
#    order : list
#        List of indices in new order.
#        ``[0,1,2,...,n]`` will yield the same matrix again.
#    
#    Returns
#    -------
#    X : np.ndarray
#        Reordered matrix.
#    """
#    nAO = matrix.shape[0]
#    I = np.identity(nAO)
#    P = I[:, order]
#    X = np.dot(np.dot(P.T, matrix), P)
#    return X
#
#def read_density(filename, alpha_only=False, header=False, debug=False):
#    """ Read density matrix from file
#    
#    Parameters
#    ----------
#    filename : str
#        Input file name.
#    alpha_only : bool
#        Whether the file contains one density matrix.
#    header : bool
#        Whether the file contains a header in the first line.
#    debug : bool
#        Whether to print status information.
#    """
#    skip = 0 if not header else 1
#    D = np.loadtxt(filename, dtype=np.float64, skiprows=skip)
#    if alpha_only:
#        nAO = int(np.sqrt(len(D)))
#        D = D.reshape((nAO, nAO))
#    else:
#        nAO = int(np.sqrt(len(D)/2))
#        D = D.reshape((2, nAO, nAO))# numpy iterates over left-most axis first
#    if debug:
#        print("-- read_density determined {0} basis functions".format(nAO))
#    return D
#
#def save_density(filename, X, alpha_only=False, header=False, debug=False):
#    major, minor = list(map(int, np.__version__.split(".")[0:2]))
#    major_good, minor_good = 1, 15
#    if major >= major_good and minor >= minor_good:
#        save_density_np(filename, X, alpha_only=alpha_only, header=header,
#                        debug=debug)
#    else:
#        print("-- Detected numpy version < 1.15.x ! Using save_density_legacy...")
#        save_density_legacy(filename, X, alpha_only=alpha_only, header=header,
#                        debug=debug)
#
#def save_density_np(filename, X, alpha_only=False, header=False, debug=False):
#    """Save density matrix tensor using numpy.
#    
#    Parameters
#    ----------
#    filename : str
#        Output file name.
#    X : np.ndarray
#        (Density) matrix
#    alpha_only : bool
#        Whether only the alpha density matrix should be written to file.
#    header : bool
#        Whether to print a header to file or not.
#    debug : bool
#        Whether to print status information.
#    """
#    h = "" if not header else " ".join([i for i in X.shape])
#    with open(filename, "w") as f:
#        if alpha_only:
#            np.savetxt(f, X, delimiter="\n", fmt="%14.10e", header=h)
#        else:
#            for slice_2d in X:
#                np.savetxt(f, slice_2d, delimiter="\n", fmt="%14.10e",
#                           header=h)
#                
#def save_density_legacy(filename, X, alpha_only=False, header=False,
#                        debug=False):
#    """Save density matrix tensor if numpy version is too old (legacy mode).
#    
#    Parameters
#    ----------
#    filename : str
#        Output file name.
#    X : np.ndarray
#        (Density) matrix
#    alpha_only : bool
#        Whether only the alpha density matrix should be written to file.
#    header : bool
#        Whether to print a header to file or not.
#    debug : bool
#        Whether to print status information.
#    """
#    h = "" if not header else " ".join([i for i in X.shape])
#    # determine number of AOs from shape --> either (nAO, nAO) or (2, nAO, nAO)
#    rows = X.shape[0] if alpha_only else X.shape[1]
#    cols = rows
#    with open(filename, "w") as f:
#        if header:
#            f.write(h)
#        if alpha_only:
#            for i in range(rows):
#                for j in range(cols):
#                    f.write("{0:.10e}\n".format(X[i,j]))
#        else:
#            for i in range(2):
#                for j in range(rows):
#                    for k in range(cols):
#                        f.write("{0:.10e}\n".format(X[i,j,k]))
#                        
def copy_density(src, dest, fragment="A", debug=False):
    """Copy the density matrix file to a new location as 'Densmat_A.txt' or
    'Densmat_B.txt'.
    
    Parameters
    ----------
    src : str
        Source file.
    dest : str
        Destination folder.
    fragment : str
        Fragment identifier ['A' or 'B']. Default: 'A'
    """
    if fragment not in ["A","B"]:
        raise ValueError("Invalid fragment specifier")
    fname = "Densmat_{0}.txt".format(fragment)
    # Check if destination folder exists
    if not os.path.exists(dest):
        os.makedirs(dest)
    # Check if source exists and proceed
    if os.path.exists(src) and os.path.isfile(src):
        shutil.copy(src, os.path.join(dest, fname))
        if debug:
            print("-- copied "+fname+" successfully.")
#
#def get_templates(method_active, isXC):
#    """Quick way to get the templates we need. Extend if necessary!
#    
#    Parameters
#    ----------
#    method_active : str
#        Method name for active species.
#    isXC : bool
#        Whether we specify XC or X+C separately.
#        
#    Returns
#    -------
#    templates : list
#        List of three templates (no_import, import_B, import_AB).
#    
#    """
#    templates = [None, None, None]
#    if method_active == "HF":
#        if isXC:
#            from CCJob.Templates import HFinHF, HFinHF_impB, HFinHF_impAB
#            templates[0] = HFinHF
#            templates[1] = HFinHF_impB
#            templates[2] = HFinHF_impAB
#        else:
#            from CCJob.Templates import HFinHF_X_C, HFinHF_impB_X_C, HFinHF_impAB_X_C
#            templates[0] = HFinHF_X_C
#            templates[1] = HFinHF_impB_X_C
#            templates[2] = HFinHF_impAB_X_C
#    elif method_active == "MP2":
#        if isXC:
#            from CCJob.Templates import ADCinHF, ADCin_impB, ADCin_impAB
#            templates[0] = ADCinHF
#            templates[1] = ADCin_impB
#            templates[2] = ADCin_impAB
#        else:
#            from CCJob.Templates import ADCinHF_X_C, ADCin_impB_X_C, ADCin_impAB_X_C
#            templates[0] = ADCinHF_X_C
#            templates[1] = ADCin_impB_X_C
#            templates[2] = ADCin_impAB_X_C
#    return templates
                        
def run_macrocycles_explicit(expansion="ME", maxiter=8, thresh=1e-8, use_zr=True,
                        fragments={}, no_mp2=True):
    """Run FDET macrocycles iterations.
    
    Parameters
    ----------
    expansion : str, optional
        FDET basis expansion (default: "ME").
    maxiter : int, optional
        Maximum number of iterations (default: 8).
    thresh : float, optional
        Macrocycle threshold w.r.t. Energy of A (default: 1e-8).
    use_zr : bool, optional
        Whether to obtain fragment structures from ZR file (default: True).
    fragments : dict, optional
        Fragment structures (key: "A" and "B") in case ZR is not used.
        
    Notes
    -----
    The module depends on the following non-standard modules which should be
    in the $PYTHONPATH:
        
    ``CCParser``, ``CCJob``
    
    Warning
    -------
    A common pitfall is a crash due to wrong charge and multiplicity with the 
    automatically generated inputs! (default: charge=0, multiplicity=1)
    
    
    """
    # Warning
    print("-"*80)
    print("[!!!] In this folder you should have:")
    print("     - Structure file      '*.zr'          (needed)")
    print("     - Density matrix of B 'Densmat_B.txt' (needed)")
    print("     - Electr. Config.     'eleconfig.txt' (optional)")
    print("-"*80)
    # Check inputs
    if expansion not in ["ME", "SE"]:
        raise ValueError(("Wrong specifier for FDET basis expansion."
                         " Use 'SE' or 'ME' only."))
    #--------------------------------------------------------------------------
    #    GENERAL STUFF
    #--------------------------------------------------------------------------
    # Iteration related
    it = 0
#    is_A = bool(it+1 % 2)
    thresh_fnt = thresh
    max_iter = maxiter
    energy = []
    iterDirName = "cy"
    iterFilName = "emb"
    subscript = "QC5sub.nov"
    
    # Important paths
    root = os.getcwd()
    path_cur = os.path.join(root, iterDirName + str(it))
#    path_old = path_cur
    
    # Filenames
    densMat_from_FDE = "FDE_State0_tot_dens.txt"
    densMat_B_init = "Densmat_B.txt"
#    densMat_B_init = "frag1_HF_dens_{0}.txt".format(expansion)
    
    #--------------------------------------------------------------------------
    #    DETERMINE STRUCTURE AND EL. CONFIGURATION
    #--------------------------------------------------------------------------
    print("*"*50)
    print("         Molecular structure and electr. configuration")
    print("*"*50)

    # XYZ structures (default: from .zr file)
    if use_zr:
        print("-- XYZ from ZR file")
        zr_file = ccj.find_file(root, extension="zr")
        frags = ccj.zr_frag(zr_file)
    else:
        print("-- XYZ from fragment input")
        frags = fragments
    print("-- Number of atoms (A): ", len(frags["A"].split("\n")))
    print("-- Number of atoms (B): ", len(frags["B"].split("\n")))

    # try to get electronic configuration
    eleconfig = {}
    valid_fnames = ["eleConf.txt", "eleconf.txt", "eleconfig.txt"]
    for fn in valid_fnames:
        F = os.path.join(root, fn)
        if os.path.exists(F) and os.path.isfile(F):
            eleconfig = ccj.electronicConf(fname=F)
            print("-- Obtained electronic configuration from file '" +fn+ "'.")
    fnb = len(eleconfig) > 0
    print("-- Charge (A)      : ", 0 if not fnb else eleconfig["charge_a"])
    print("-- Multiplicity (A): ", 1 if not fnb else eleconfig["multiplicity_a"])
    print("-- Charge (B)      : ", 0 if not fnb else eleconfig["charge_b"])
    print("-- Multiplicity (B): ", 1 if not fnb else eleconfig["multiplicity_b"])

    # Create and change into first folder
    if not os.path.exists(path_cur):
        os.makedirs(path_cur)
    os.chdir(path_cur)
    #------------------------------------------------------------------------------
    #         FIRST JOB
    #------------------------------------------------------------------------------
    print_iteration(it)
    # general info
    curr_inp = iterFilName+".in"
    currID = "MC{0}".format(it)
    
    # copy density matrix (B)
    shutil.copy(os.path.join(root, densMat_B_init), path_cur)
    
    # prepare input from template
    from CCJob.Templates import HFinHF_impB_X_C, HFinHF_impAB_X_C
    #from CCJob.Templates import ADCin_impB, ADCin_impAB
    specs = {"memory": 20000,
             "frag_a": frags["A"],
             "frag_b": frags["B"],
             "t_func": "TF",
             "x_func": "Slater",
             "c_func": "VWN5",
             "expansion": expansion
             }
    specs.update(eleconfig)
    inp = ccj.Input.from_template(HFinHF_impB_X_C, curr_inp, **specs)
    
    # Prepare job 
    slurm = {"mem":       25000,
             "cpus":      8,
             "time":      2,#2 minutes
             "partition": "debug",
             "jobname":   currID}
    job = ccj.Job(inp, script=subscript, **slurm)
    slurm_custom = {"--nodes":           1,
                    "--ntasks-per-node": 1,
                    "--mail-user":       "alexzech777@gmail.com",
                    "--mail-type":       "END"}
    job.set_custom_options(**slurm_custom)
    
    # Run the first job
    job.run()
    
    # Parse output
    out = ccp.Parser(iterFilName+".out", software="qchem",
                     to_console=False, to_file=True,
                     logname="parser_{0}.log".format(it))
    
    # get HF energy (HF-in-x)
    energy.append(out.results.hf_energy.get_last())
    
    # get number of basis functions from the basis initializer block (constructor)
    nbasAB = out.results.nbas[2]
    nbasA  = out.results.nbas[3]
    nbasB  = out.results.nbas[4]
    print("""-- Found number of basis functions:
       .nbasAB = {0}
       .nbasA  = {1}
       .nbasB  = {2}
    """.format(nbasAB, nbasA, nbasB))
    
    # no need to read and transform. Just copy
    
    # adjust iteration counter
    it += 1
    #------------------------------------------------------------------------------
    #         ALL OTHER JOBS
    #------------------------------------------------------------------------------
    difference_to_last = 1
    while abs(difference_to_last) > thresh_fnt and it < max_iter:
        # iteration variables
#        is_A = bool((it+1) % 2)
        print_iteration(it)
        
        # Folders
        path_old = path_cur
        path_cur = os.path.join(root, iterDirName + str(it))
        if not os.path.exists(path_cur):
            os.makedirs(path_cur)
        os.chdir(path_cur)
        
        # copy density matrix from last iteration
        lastDensityA = os.path.join(path_old, densMat_from_FDE)
        copy_density(lastDensityA, path_cur, fragment="A", debug=True)
        
        # copy density matrix of subsystem B
        shutil.copy(os.path.join(root, densMat_B_init),
              os.path.join(path_cur, densMat_B_init))
        
        # Prepare input
        curr_inp = iterFilName+".in"
        
        # create input object
        inp = ccj.Input.from_template(HFinHF_impAB_X_C, curr_inp, **specs)
        
        # Prepare job  (like above)
        job = ccj.Job(inp, script=subscript, **slurm)
        job.set_custom_options(**slurm_custom)
    
        # Run the iterative job
        job.run()
        
        # Parse output
        out = ccp.Parser("fnt_{0}.out".format(it), software="qchem",
                         to_console=False, to_file=True,
                         logname="parser_{0}.log".format(it))
        
        # get HF energy (HF-in-x)
        energy.append(out.results.mp_energy.get_last())
        difference_to_last = energy[it] - energy[it-1]
        print("-- Current residual: {0: .8e}".format(difference_to_last))
        
        # increase iteration counter
        it += 1
    print("*"*80)
    print("[!] Finished FDET macrocycles calculation successfully in {0} iterations.".format(it))
    
    if not no_mp2:
        append_MP2(specs, root, it-1)
        
def append_MP2(specs, rootDir, lastIter):
    # Kickass banner
    print_banner("FDE-MP2 calculation")
    
    # General stuff
    subscript = "QC5sub.nov"
    densMat_from_FDE = "FDE_State0_tot_dens.txt"
    
    # Create and change into MP2 folder
    mp2path = os.path.join(rootDir, "MP2")
    if not os.path.exists(mp2path):
        os.makedirs(mp2path)
    os.chdir(mp2path)

    # Prepare MP2 input
    curr_inp = "emb.in"
    currID = "FDE-MP2"
    
    # copy converged density matrix (A)
    lastDir = os.path.join(rootDir, "cy{0}".format(lastIter))
    lastDensityA = os.path.join(lastDir, densMat_from_FDE)
    copy_density(lastDensityA, mp2path, fragment="A", debug=True)
    
    # copy density matrix (B)
    shutil.copy(os.path.join(rootDir, "Densmat_B.txt"), mp2path)
    
    from CCJob.Templates import ADCin_impB_X_C
    mp2 = {"nstates" : 0}
    specs.update(mp2)
    inp = ccj.Input.from_template(ADCin_impB_X_C, curr_inp, **specs)
    
    # Prepare job 
    slurm = {"mem":       50000,
             "cpus":      16,
             "time":      45,
             "partition": "wesolowski",
             "jobname":   currID}
    job = ccj.Job(inp, script=subscript, **slurm)
    slurm_custom = {"--nodes":           1,
                    "--ntasks-per-node": 1,
                    "--mail-user":       "alexzech777@gmail.com",
                    "--mail-type":       "END"}
    job.set_custom_options(**slurm_custom)
    
    # Run the first job
    job.run()
    
    print_banner("[!] Finished FDE-MP2 calculation.")
    
######
    
def macroCycles(specs, queue, method_A="HF", method_B="import", maxiter=8,
                thresh=1e-8, use_zr=True, fragments={}, lw=80, B_init=False,
                q_custom=None):
    """Run FDET macrocycles iterations.
    
    Parameters
    ----------
    specs : :obj:`dict`
        Dictionary containing values to create the input from template.
    queue : :obj:`dict`
        Dictionary for queuing manager variables (see :class:`CCJob.Job`).
    method_A : :obj:`str`
        Quantum chemical method abbrev. for system A (default: 'HF').
    method_B : :obj:`str`
        Quantum chemical method abbrev. for system B (default: 'HF').
    maxiter : :obj:`int`, optional
        Maximum number of iterations (default: 15).
    thresh : :obj:`float`, `optional`
        Freeze-and-thaw threshold w.r.t. Energy of A (default: 1e-8).
    use_zr : :obj:`bool`, optional
        Whether to obtain fragment structures from ZR file (default: True).
    fragments : :obj:`dict`, optional
        Fragment structures (key: "A" and "B") in case ZR is not used.
    lw : :obj:`int`
        Line width (for print function)
    B_init : :obj:`bool`
        Whether an initial density matrix for B is provided.
    q_custom : :obj:`dict`, optional
        Custom options for queuing manager (see
        :func:`CCJob.Job.set_custom_options`)
        
    Notes
    -----
    The module depends on the following non-standard modules which should be
    in the $PYTHONPATH:
        
    ``CCParser``, ``CCJob``
    
    
    Warning
    -------
    A common pitfall is a crash due to wrong charge and multiplicity with the 
    automatically generated inputs! (default: charge=0, multiplicity=1)
    
    
    """
    # Warning
    print("-"*lw)
    print("[!!!] In this folder you should have:")
    print("     - Structure file      '*.zr'          (needed)")
    print("     - Density matrix of B 'Densmat_B.txt' (needed)")
    print("     - Electr. Config.     'eleconfig.txt' (optional)")
    print("-"*lw)
    
    # Check inputs
    if "expansion" not in specs.keys():
        raise KeyError("FDET basis expansion not defined!!")
    elif specs["expansion"] not in ["ME", "SE"]:
        raise ValueError(("Wrong specifier for FDET basis expansion."
                         " Use 'SE' or 'ME' only."))
    if method_A not in ["HF", "MP2"]:
        raise ValueError("Descriptor for 'method_A' not recognized!")
    elif method_B not in ["HF", "MP2", "import"]:
        raise ValueError("Descriptor for 'method_B' not recognized!")
    #--------------------------------------------------------------------------
    #    Determine Macrocycle parameters
    #--------------------------------------------------------------------------
    isXC = False if not "xc_func" in specs.keys() else True
    if B_init:
        method_B = "import"
#    method_A = "MP2"
#    method_B = "MP2"
    
        
    #--------------------------------------------------------------------------
    #    GENERAL STUFF
    #--------------------------------------------------------------------------
    # Iteration related
    it = 0
    thresh_fnt = thresh
    max_iter = maxiter
    energy = []
    
    # General
    iterDirName = "cy"
    iterFilName = "emb"
    
    # Important paths
    root = os.getcwd()
    path_cur = os.path.join(root, iterDirName + str(it))
#    path_old = path_cur
    
    # Density matrix filenames
    densMat_from_FDE = "FDE_State0_tot_dens.txt"
    if B_init:
        densMat_B = "Densmat_B.txt"
    else:
        #TODO catch correct theory descriptor! now hardcoded to HF
        densMat_B = "frag1_{0}_dens_{1}.txt".format("HF", specs["expansion"])
    
    #--------------------------------------------------------------------------
    #    DETERMINE STRUCTURE AND EL. CONFIGURATION
    #--------------------------------------------------------------------------
    print_banner("Molecular structure and electr. configuration")

    # XYZ structures (default: from .zr file)
    if use_zr:
        print("-- XYZ from ZR file")
        zr_file = ccj.find_file(root, extension="zr")
        frags = ccj.zr_frag(zr_file)
    else:
        print("-- XYZ from fragment input")
        frags = fragments
    print("-- Number of atoms (A): ", len(frags["A"].split("\n")))
    print("-- Number of atoms (B): ", len(frags["B"].split("\n")))

    # try to get electronic configuration
    eleconfig = {}
    valid_fnames = ["eleConf.txt", "eleconf.txt", "eleconfig.txt"]
    for fn in valid_fnames:
        F = os.path.join(root, fn)
        if os.path.exists(F) and os.path.isfile(F):
            eleconfig = ccj.electronicConf(fname=F)
            print("-- Obtained electronic configuration from file '" +fn+ "'.")
    fnb = len(eleconfig) > 0
    print("-- Charge (A)      : ", 0 if not fnb else eleconfig["charge_a"])
    print("-- Multiplicity (A): ", 1 if not fnb else eleconfig["multiplicity_a"])
    print("-- Charge (B)      : ", 0 if not fnb else eleconfig["charge_b"])
    print("-- Multiplicity (B): ", 1 if not fnb else eleconfig["multiplicity_b"])
    
    # update input dictionary with structure and electronic configuration
    specs.update(eleconfig)
    specs.update(dict(frag_a=frags["A"], frag_b=frags["B"]))

    # Create and change into first folder
    if not os.path.exists(path_cur):
        os.makedirs(path_cur)
    os.chdir(path_cur)
    #------------------------------------------------------------------------------
    #         FIRST JOB
    #------------------------------------------------------------------------------
    print_iteration(it)
    # general info
    t_idx    = 0 if not B_init else 1
    curr_inp = iterFilName+".in"
    curr_job = "MC-{0}".format(it)
    
    # copy density matrix (B)
    if B_init:
        shutil.copy(os.path.join(root, densMat_B), path_cur)
    
    # prepare input from template
    templates = get_templates(method_A, isXC)    
    inp = ccj.Input.from_template(templates[t_idx], curr_inp, **specs)
    
    # Prepare job 
#    slurm = {"mem":       25000,
#             "cpus":      8,
#             "time":      2,#2 minutes
#             "partition": "debug",
#             "jobname":   currID}
    queue["jobname"] = curr_job
    job = ccj.Job(inp, **queue)
    if q_custom != None:
        job.set_custom_options(**q_custom)
    
    # Run the first job
    job.run()
    
    # Parse output
    out = ccp.Parser(iterFilName+".out", software="qchem",
                     to_console=False, to_file=True, json=True,
                     log_file="parser_{0}.log".format(it),
                     json_file="parser_{0}.json".format(it))
    
    # get embedded energy of active system
    # the order is generally HF(0), post-HF(1->nstates)
    if len(out.results.E_A) <= 2:
        energy.append(out.results.E_A.get_last())
    elif len(out.results.E_A) > 2:
        # if for some reason we also calculate excited states
        energy.append(out.results.E_A[1])
    
    # get number of basis functions from the basis initializer block (constructor)
    nbasAB = out.results.nbas[2]
    nbasA  = out.results.nbas[3]
    nbasB  = out.results.nbas[4]
    print("""-- Found number of basis functions:
       .nbasAB = {0}
       .nbasA  = {1}
       .nbasB  = {2}
    """.format(nbasAB, nbasA, nbasB))
    
    # no need to read and transform. Just copy
    
    # adjust iteration counter
    it += 1
    #------------------------------------------------------------------------------
    #         ALL OTHER JOBS
    #------------------------------------------------------------------------------
    difference_to_last = 1
    while abs(difference_to_last) > thresh_fnt and it < max_iter:
        # iteration variables
        curr_job = "MC{0}".format(it)
        
#        is_A = bool((it+1) % 2)
        print_iteration(it)
        
        # Folders
        path_old = path_cur
        path_cur = os.path.join(root, iterDirName + str(it))
        if not os.path.exists(path_cur):
            os.makedirs(path_cur)
        os.chdir(path_cur)
        
        # copy density matrix/-ices from last iteration
        lastDensityA = os.path.join(path_old, densMat_from_FDE)
        copy_density(lastDensityA, path_cur, fragment="A", debug=True)
        if B_init:
            lastDensityB = os.path.join(root, densMat_B)
            shutil.copy(lastDensityB, os.path.join(path_cur, densMat_B))
        else:
            lastDensityB = os.path.join(root, iterDirName + str(0))
            copy_density(lastDensityB, path_cur, fragment="B", debug=True)
        
        # Prepare input
        curr_inp = iterFilName+".in"
        
        # create input object
        inp = ccj.Input.from_template(templates[2], curr_inp, **specs)
        
        # Prepare job  (like above)
        queue["jobname"] = curr_job
        job = ccj.Job(inp, **queue)
        if q_custom != None:
            job.set_custom_options(**q_custom)
    
        # Run the iterative job
        job.run()
        
        # Parse output
        out = ccp.Parser(iterFilName+".out", software="qchem",
                         to_console=False, to_file=True, json=True,
                         logname="parser_{0}.log".format(it),
                         json_file="parser_{0}.json".format(it))
        
        # get embedded energy (see above)
        if len(out.results.E_A) <= 2:
            energy.append(out.results.E_A.get_last())
        elif len(out.results.E_A) > 2:
            energy.append(out.results.E_A[1])
        difference_to_last = energy[it] - energy[it-1]
        print("-- Current residual: {0: .8e}".format(difference_to_last))
        
        # increase iteration counter
        it += 1
        # END WHILE LOOP
        
    print("*"*lw)
    print("[!] Finished FDET macrocycles calculation successfully in {0} iterations.".format(it))
    
###############################################################################
###############################################################################
#if __name__ == "__main__":
#    import argparse
#    arg_parser = argparse.ArgumentParser(description=("Run FDET macrocycle"
#                                         " calculations with Q-Chem."))
#    # positional arguments
#    #arg_parser.add_argument("outFiles", metavar="out", nargs="+", help="outputfiles")
#    # FDET basis expansion
#    arg_parser.add_argument("-S", "--super", action="store_const",
#                            const="SE", default="ME", dest="expansion",
#                            help=("use supermol. expansion (default: monomer"
#                            " expansion)"))
#    # MC threshold
#    arg_parser.add_argument("-t", "--threshold", action="store", default=1e-8,
#                            type=float, dest="thr",
#                            help=("Threshold for change of Energyof A "
#                                  "(default: 1e-8)"))
#    # MC maxiter
#    arg_parser.add_argument("-i", "--maxiter", action="store", default=8,
#                            type=int, dest="maxiter",
#                            help=("set maximum macrocycle iterations "
#                                  "(default: 8)"))
#    # append MP2 calc?
#    arg_parser.add_argument("-M", "--without-mp2", action="store_const",
#                            const=True, default=False,
#                            type=bool, dest="no_mp2",
#                            help=("whether not to run MP2 after FDE-HF "
#                                  "converges (default: False)"))
#    # version info
#    arg_parser.add_argument("-v", "--version", action="version",
#                        version="%(prog)s | Version: {0}\n {1}, {2}".format(
#                                info["version"], info["author"], info["date"]),
#                        help="show version info")
#    args = arg_parser.parse_args()
#
#
#    run_macrocycles_explicit(expansion=args.expansion, maxiter=args.maxiter, 
#                    thresh=args.thr, no_mp2=args.no_mp2)
#    
